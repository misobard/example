<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="name" value="ミナト"/>
<c:set var="age" value="23"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<p><c:out value="${name}"/></p>
<p><c:out value="${age}"/></p>
<c:if test="${age>=20}">
	あなたは成人です
	</c:if>
<c:choose>
	<c:when test="${age>=50}">  
	あなたは成人です
	</c:when>
	<c:otherwise>
	あなたは未成年です
	</c:otherwise>
	</c:choose>

	<c:forEach var="i" begin="0" end="9" step="1">
	<c:out value="${i}"/>
	</c:forEach>
</body>
</html>
<%--  --%>


