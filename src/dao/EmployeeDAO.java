package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Employee;
public class EmployeeDAO {

	//-----JDBCコピペ1ここから-----------------------
    final String DRIVER_NAME = "com.mysql.jdbc.Driver";// MySQLドライバ
    final String DB_URL = "jdbc:mysql://localhost:3306/";// DBサーバー名
    final String DB_NAME = "example";// データベース名
    final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";// 文字化け防止
    final String JDBC_URL = DB_URL + DB_NAME + DB_ENCODE;// 接続DBとURL
    final String DB_USER = "root";// ユーザーID
    final String DB_PASS = "root";// パスワード
    // -------JDBCコピペ1ここまで------------------------

    public List<Employee> findAll(){
    	List<Employee> empList=new ArrayList();

    	 // JDBCコピペ2ここから--------------------------------
        try

       	 {
       	  Class.forName(DRIVER_NAME);


		}catch(
			ClassNotFoundException e)
			// TODO: handle exception
        {
       	 e.printStackTrace();//エラー内容を表示
        }
     // JDBCコピペ2ここまで--------------------------------

        try (Connection conn = DriverManager.getConnection(
        JDBC_URL,DB_USER,DB_PASS)) {

        //SELECT文を準備
        String sql="SELECT ID,NAME,AGE FROM EMPLOYEE";
        PreparedStatement pStmt=conn.prepareStatement(sql);

        //SELECTを実行し,結果票を取得
        ResultSet rs = pStmt.executeQuery();

        //結果票に格納されたレコードの内容を
        //Employeeインスタンスに設定し、ArrayListインスタンスに追加
        while(rs.next()){
        	String id=rs.getString("ID");
        	String name=rs.getString("NAME");
        	int age =rs.getInt("AGE");
        	Employee employee= new Employee(id,name,age);
        	empList.add(employee);
        }

		} catch (Exception e) {
			e.printStackTrace();
			return null;
			// TODO: handle exception
		}
		return empList;

    }
    public List<Employee> findById(String x){
    	List<Employee> empList=new ArrayList();

    	 // JDBCコピペ2ここから--------------------------------
        try

       	 {
       	  Class.forName(DRIVER_NAME);


		}catch(
			ClassNotFoundException e)
			// TODO: handle exception
        {
       	 e.printStackTrace();//エラー内容を表示
        }
     // JDBCコピペ2ここまで--------------------------------

        try (Connection conn = DriverManager.getConnection(
        JDBC_URL,DB_USER,DB_PASS)) {

        String sql="SELECT ID,NAME,AGE FROM EMPLOYEE WHERE ID=?";
        PreparedStatement pStmt=conn.prepareStatement(sql);
        pStmt.setString(1, x);

        ResultSet rs = pStmt.executeQuery();
        while(rs.next()){
        	String id=rs.getString("ID");
        	String name=rs.getString("NAME");
        	int age =rs.getInt("AGE");
        	Employee employee= new Employee(id,name,age);
        	empList.add(employee);
        }
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			// TODO: handle exception
		}
		return empList;

    }

    public boolean create(Employee employee){


    	 // JDBCコピペ2ここから--------------------------------
        try

       	 {
       	  Class.forName(DRIVER_NAME);


		}catch(
			ClassNotFoundException e)
			// TODO: handle exception
        {
       	 e.printStackTrace();//エラー内容を表示
        }
     // JDBCコピペ2ここまで--------------------------------

        try (Connection conn = DriverManager.getConnection(
        JDBC_URL,DB_USER,DB_PASS)) {


        String sql="INSERT INTO  employee (id,name,age) VALUES (?,?,?)";
        PreparedStatement pStmt=conn.prepareStatement(sql);
        pStmt.setString(1,employee.getId());
        pStmt.setString(2,employee.getName());
        pStmt.setInt(3,employee.getAge());

        int result= pStmt.executeUpdate(); //成功したら1が返ってくる失敗したら０が返ってくる
        if(result !=1){
        	return false;
        }
        ResultSet rs = pStmt.executeQuery();

		} catch (Exception e) {
			e.printStackTrace();
			return false;

		}
		return true;

    }


}
